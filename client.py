import socket
import sys
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-n', '--hostname', help="Hostname or IP to connect to.")
parser.add_argument('-p', '--port', type=int, help="Port to connect on")
args = parser.parse_args()

if hasattr(args, 'hostname'):
    host = args.hostname
else:
    host = ''

if hasattr(args, 'port'):
    port = args.port
else:
    port = 9876

bufSize = 4096
serverSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serverSock.connect((host, port))
nuCode = '%u'
auCode = '%a'
delimeter = ':::'

class System_Code(object):
    name = ''

    # handle invalid username function
    def new_username(self):
        sys.stdout.write('Please choose a unique username: ')
        sys.stdin.flush()
        self.name = sys.stdin.readline()
        self.name = self.name[:-1]
        serverSock.send(self.name + delimeter  + "\n")

s = System_Code()

authorized = False
try:
    s.new_username()
    while not(authorized):
        rcvMessage = serverSock.recv(bufSize)
        # take out message code
        pMessage = rcvMessage.split(delimeter)
        # handle new username code
        if pMessage[0] == nuCode:
            s.new_username()
        if pMessage[0] == auCode:
            authorized = True;


except KeyboardInterrupt:
    print '\n'
    serverSock.close()
    sys.exit()


pid = os.fork()

if not pid:
    while 1:
        try:
            rcvMessage = serverSock.recv(bufSize)
            if not rcvMessage:
                    print '\nDisconnected :('
                    serverSock.close()
                    os.killpg(os.getpgid(os.getppid()), 15)
            else:
                sys.stdout.write(rcvMessage)
        except KeyboardInterrupt:
            print '\n'
            serverSock.close()
            os.killpg(os.getpgid(os.getppid()), 15)

else:
    while 1:
        try:
            message = sys.stdin.readline()
            serverSock.send(s.name + delimeter + message)
        except KeyboardInterrupt:
            serverSock.close()
            os.killpg(os.getpgid(os.getppid()), 15)


