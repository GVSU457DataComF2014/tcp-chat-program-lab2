You may write this program in C, or C++, Python, or Java. Other languages may be allowed by request. You may not use any non-standard libraries without prior permission. Your program must work on the computers in the Datacomm lab. You may program it wherever you like, but must demo it in the datacomm lab, between multiple lab computers.

Your assignment is to write both the server and client parts of a chat program.
You must use TCP sockets. You must support the following functionality:

* The server should allow for multiple simultaneous clients, with individual
users identified by a username.
* The server should provide a way for the clients to get a list of all other
connected clients/users.
* The server should support, clients sending to individual other clients, clients
sending to all other online users simultaneiously, or clients sending to some
group (group membership should be tracked by the server).
* The server should support administrative commands (sent from a client). At
minimum, you should implement a command to kick off another user, and commands
to handle groups (create, join, etc). It is up to you how these will be
distinguised from text.
* The client should provide a frontend to access all funtionality the server
supports.
* Unexpected loss of a connection should be handled gracefully by both the client
and server.
As in the previous project, please fill out group evaluation forms.

░░░░░░░░░░░▓███████▓░░░░░░░░░░

░░░░░░░░▓██▓▓▒▒▒▒▒▓▓██▓░░░░░░░

░░░░░▒██▓▒░░░░░░░░░░░▒▓██▒░░░░

░░░░██▒░░░░░░░░░░░░░░░░░▓██░░░

░░░██▒▒▒▒░░░░░░░░░▓▓▓▒░░░░██░░

░░█▓▓▓░▓██▒░░░░░▒█▓▒▒██▓░░░██░

░██▓▓░░▓███░░░░▒█░░░░███▓░░░█▓

▒█░█░░░░░░█▓░░░▓▓░░░░░░░█░░░▒█

██░▓█▓▓▓▓▓█▒░░░▒█▓▓▓▓▓▓▓▓░░░░█

██░░░░░░░░░░░░░░░░░░░░░░░░░░░█

██░░▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓░░░░░█

░█▒░░███████████████████▓░░░▒█

░▓█░░▒█▓▓▓▓▓▓▓▓█████▓▓▓█▓░░░█▓

░░██░░▓█▓▓▓▓▓██▓▓▓▓██▓██░░░██░

░░░██░░▓██▓▓█▓░░░░░░▒██▒░░██░░

░░░░██▓░░▓██▓░░░░░▒▓▓▓░░▓██░░░

░░░░░▒██▓░░▒▒▓▓▓▓▓▓▒░░▓██▒░░░░

░░░░░░░░▒▓▓▓▓▓▓▓▓▓▓▓▓▓▒░░░░░░░ 