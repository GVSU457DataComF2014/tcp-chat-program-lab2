from asyncore import dispatcher
from asynchat import async_chat
import socket
import asyncore
import random

PORT = 9876
NAME = 'ChatLine'
users = {}
welcomeBanner = '#### Welcome ####'
nuCode = '%u'
auCode = '%a'
lgCode = '%listGroups'    # list groups
agCode = '%makeGroup'      # add group
rgCode = '%removeGroup'  # remove groups
sgCode = '%sendGroup'     # send to all members of group
apgCode = '%addTo'        # add a person to group
rpgCode = '%removeFrom'   # remove person from group
lcCode = '%listCommands'
maCode = '%setAdmin'
cgCode = '%changeGroup'   # changing yourself does not req admin
wsCode = '%whisper'
ylCode = '%yell'
kkCode = '%kick'
luCode = '%listUsers'
isACode = '%admin'

commands = {lgCode:  "List groups.",
            agCode:  "[group] Makes a group only you have access to, you must switch to group",
            rgCode:  "[group] Removes entire group",
            sgCode:  "[group] [message] Sends message to group you have access to but are not in",
            apgCode: "[username] [group] Gives username's access to group",
            rpgCode: "[username] [group] Removes username's access to group",
            lcCode:  "Lists the commands",
            maCode:  "[username] [1 or 0] Grants (1) or removes (0) username's Admin status",
            cgCode:  "[group] Make group your active group",
            wsCode:  "[username] Sends message only to username",
            ylCode:  "[message] Sends message to all users",
            kkCode:  "[username] [message] Kicks username and sends them message",
            luCode:  "[group] optional argument, displays everyone by default",
            isACode: "returns whether or not you are an admin "}


def praseCommands(curSession, cline):
    userName = cline[0]
    sline = cline[1].split(" ")
    print sline
    if sline[0] in commands:
        iscommand = True
        # print "Made it to: recognized command "
        print "command is: " + sline[0]
        # List Groups
        if sline[0] == lgCode:
            print "Made it to: list groups "
            for g in users[curSession][GROUP]:
                if g == users[curSession][ACTGR]:
                    curSession.push('*'+g+'\n')
                else:
                    curSession.push(g+'\n')
        # add group to self only
        elif sline[0] == agCode and users[curSession][ADMIN]:
            print "Made it to: made it to add group "
            users[curSession][GROUP].append(sline[1])
            curSession.push('Made Group : '+sline[1]+'\n')
        # remove group from all users
        elif sline[0] == rgCode and users[curSession][ADMIN]:
            print "Made it to: remove group "
            if not(sline[1] == 'default'):
                print "NOT trying to delete default"
                for session in users.keys():
                    if sline[1] in users[session][GROUP]:
                        print "found group somewhere"
                        index = users[session][GROUP].index(sline[1])
                        # Switches user to default if removed from active group
                        if users[session][ACTGR] == sline[1]:
                            print "Switched user: "+users[session][USERN]
                            print "from group: "+users[session][ACTGR]
                            users[session][ACTGR] = "default"
                            print "to Group: "+users[session][ACTGR]
                        del users[session][GROUP][index]

        # send message to another group
        elif sline[0] == sgCode and users[curSession][ADMIN]:
            print "Made it to: send message to another group"
            #Checks if Admin is a part of group, if so sends message to group
            if sline[1] in users[curSession][GROUP]:
                message = " ".join(sline[2:])
                curSession.server.broadcast(message, curSession, sline[1])
            else:
                curSession.push("You do not have access to that group!\n")

        # add person to group
        elif sline[0] == apgCode and (users[curSession][ADMIN] or
                    userName == users[curSession][USERN]):
            print "Made it to: add person to group"
            # adding oneself still loops through all users TODO
            for session in users.keys():
                if sline[1] == users[session][USERN]:
                    users[session][GROUP].append(sline[2])
        # remove a person from group
        elif sline[0] == rpgCode and (users[curSession][ADMIN] or
                sline[1]==users[curSession][USERN]):
            print "Made it to: remove person from group"
            #check if group exists
            for session in users.keys():
                if users[session][USERN] == sline[1]:
                    # group exists remove from users list
                    if sline[2] in users[session][GROUP]:
                        # Switches user to default if removed from active group
                        if users[session][ACTGR] == [sline[1]]:
                            users[session][ACTGR] = "default"
                        users[session][GROUP].remove(sline[2])
        # set admin make specified user admin
        elif sline[0] == maCode and users[curSession][ADMIN]:
            print "Made it to: make admin"
            for session in users.keys():
                if users[session][USERN]==sline[1]:
                    users[session][ADMIN] = int(sline[2])
                    print "toggle admin"+users[session][USERN]+" to" + sline[2] + "\n"
        # change self group
        elif sline[0] == cgCode:
            print "Made it to: change current group"
            users[curSession][ACTGR] = sline[1]
        # whisper to a user
        elif sline[0] == wsCode:
            print "Made it to: whisper"
            for session in users.keys():
                if users[session][USERN]==sline[1]:
                    session.push("(whisper)"+ users[curSession][USERN] + delimeter +" ".join(sline[2:])+'\n')
        
        # yell at all users across groups
        elif sline[0] == ylCode:
            print "Made it to: yell"
            for session in users.keys():
                session.push("(YELL)"+ users[curSession][USERN] + delimeter +" ".join(sline[1:]).upper()+'\n')
        #kick specified user
        elif sline[0] == kkCode:
            print "Made it to: kick"
            for session in users.keys():
                if users[session][USERN] == sline[1]:
                    session.push("(kicked by)" + users[curSession][USERN] +
                            " for the following reason: " + " ".join(sline[2:])+'\n')
                    session.handle_close()
    
        
        # list users (group if given group name, else all users on server)
        elif sline[0] == luCode:
                try:
                    if sline[1] in users[curSession][GROUP]:
                        curSession.push("All users in group: " + sline[1] + '\n')
                        for sesh in users.keys():
                            if sline[1] in users[sesh][GROUP]:
                                curSession.push(users[sesh][USERN]+'\n')
                except IndexError as e:
                    curSession.push("All users \n")
                    for session in users.keys():
                        curSession.push(users[session][USERN]+'\n')
        # Am I an Admin
        elif sline[0] == isACode:
            if users[curSession][ADMIN]:
                curSession.push("You are an Admin")
            else:
                curSession.push("You are not an Admin")

       # list commands
        elif sline[0] == lcCode:
            print "Made it to: List commands"
            for com in commands:
                curSession.push(com)
                curSession.push(commands[com]+'\n')
    curSession.push('\n========================\n')

delimeter = ':::' 
USERN = 0
ADMIN = 1
AUTHO = 2
GROUP = 3
ACTGR = 4

class ChatSession(async_chat):

    def __init__(self, server, sock):
        async_chat.__init__(self, sock)
        self.server = server
        self.set_terminator("\n")
        self.data = []

    def collect_incoming_data(self, data):
        self.data.append(data)

    def found_terminator(self):
        line = "".join(self.data)
        print 'MESSAGE RECIEVED: ' + line #DEBUG
        sline = line.split(delimeter)  # Split username/data
        print 'USERNAME/MESSGE SPLIT: ' + ", ".join(sline) + '\n'  #DEBUG
        #DEBUG
        ###################################
        # Handle user codes
        ###################################
        iscommand = False
        command = sline[1].split(" ")[0]
        print "Possible Command " + command
        if command in commands:
            praseCommands(self, sline)
            iscommand = True

        # user is added && authorized -> broadcast message
        if sline[0] == users[self][USERN] and users[self][AUTHO] and not iscommand:
            print 'corrrect username: '+ sline[0]+  ' about to broadcast: '+line #DEBUG
            self.server.broadcast(line, self,users[self][ACTGR])
        ################################################################
        # invalid username handling
        ################################################################
        elif not iscommand:
            print 'determining if name: '+sline[0] +' is clear to use' #DEBUG
            appears = False
            for session in users.keys():
                # if username is in use && this user is not authorized
                if sline[0] == users[session][USERN] and not(users[self][AUTHO]):
                    print 'username found: issuing new user code' #DEBUG
                    # TODO above second condition may be extraneous
                    # new username code sent to client
                    self.push(nuCode + delimeter+'\n')
                if sline[0] == users[session][USERN]:
                    print 'username found' #DEBUG
                    appears = True
            if not(appears) and not(users[self][AUTHO]):
                print 'username free : assigning username' #DEBUG
                users[self][USERN] = sline[0]  # Set user name
                users[self][AUTHO] = True
                self.push(auCode+delimeter+"You have been added to the server as "+sline[0]+'\n')
        print "Made it through the ifs"
        self.data = []
        line = ''
        sline = []

    def handle_close(self):
        async_chat.handle_close(self)
        self.server.disconnect(self)


class ChatServer(dispatcher):

    def __init__(self, port, name):
        dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind(('0.0.0.0', port))
        self.listen(5)
        self.name = name
        self.sessions = []

    def disconnect(self, session):
        self.sessions.remove(session)
        del users[session]
        # if they are the only admin call quiz game for remaining
        # users to see who gets to be the admin

    def broadcast(self, line, skip, actg):
        for session in self.sessions:
            # print session
            if session is skip or not(users[session][ACTGR] ==  actg):
                pass
            else:
                # Display Admin 'A' after username
                if users[skip][ADMIN]:
                    sline = line.split(delimeter)
                    session.push(sline[0]+'\033[1m' + '[A]' + '\033[0m'+ delimeter+sline[1]+ '\n')
                else: 
                    session.push(line + '\n')
                
    def handle_accept(self):
        conn, addr = self.accept()
        print "Connected from " + str(addr)
        cs = ChatSession(self, conn)
        self.sessions.append(cs)
        # create list value for users dict
        username = ''
        authorized = 0
        groups = ['default']
        activegroup = 'default'
        # check for admins, if none make one
        if len(users.keys()) == 0:
            print 'Made an ADMIN'
            admin = 1
        else:
            admin = 0
        lv = [ username, admin, authorized, groups,activegroup] # admin set false, groups set
        # add session as key in users
        users.setdefault(cs,lv)


if __name__ == '__main__':
    s = ChatServer(PORT, NAME)
    try:
        asyncore.loop()
    except KeyboardInterrupt:
        print
